﻿using Grpc.Core;
using Grpc.Net.Client;
using System;

namespace Tema2CNAClient
{
    class Program
    {
        private static GrpcChannel channel;
        private static GetZodiacService.GetZodiacServiceClient client;
        static void RunMainMenu()
        {
            char option = ' ';
            do
            {
                try
                {
                    Console.WriteLine("1 - Trimite date catre server");
                    Console.WriteLine("2 - Iesire aplicatie");
                    option = Console.ReadKey().KeyChar;
                    Console.Clear();
                    switch (option)
                    {
                        case '1':
                            {
                                Console.WriteLine("\nIntroduceti data de nastere a clientului clientului:");
                                var date = Console.ReadLine();

                                if (String.IsNullOrWhiteSpace(date))
                                {
                                    Console.WriteLine("\nSirul inrodus este vid!");
                                    break;
                                }

                                if(date.Length>=11)
                                {
                                    Console.WriteLine("\nSirul introdus este prea lung pentru a reprezenta o data valida!");
                                    break;
                                }

                                var result = client.GetZodiacDataRequest(new Date
                                {
                                    CalendaristicDate = date
                                });

                                if (result.ZodiacName.ToString() != String.Empty)
                                {
                                    Console.WriteLine("\nClientul are zodia " + result.ZodiacName.ToString()+"\n\n");
                                }
                                break;
                            }

                        case '2':
                            {
                                Console.WriteLine("\nClientul a fost inchis!\n\n");
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("\nOptiune invalida!\n\n");
                                break;
                            }
                    }
                }
                catch (RpcException e)
                {
                    Console.WriteLine(e.Status.Detail);
                }
            } while (option != '2');
        }

        static void Main(string[] args)
        {
            channel = GrpcChannel.ForAddress("https://localhost:5001");
            client = new GetZodiacService.GetZodiacServiceClient(channel);

            RunMainMenu();

            channel.ShutdownAsync();
        }
    }
}
