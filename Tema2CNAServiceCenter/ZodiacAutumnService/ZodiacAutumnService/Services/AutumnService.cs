using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacAutumnService
{
    public class AutumnService : GetAutumnZodiacService.GetAutumnZodiacServiceBase
    {
        private readonly ILogger<AutumnService> _logger;
        public AutumnService(ILogger<AutumnService> logger)
        {
            _logger = logger;
        }

        private ZodiacName GetAutumnZodiacName(int day, int month, int year)
        {
            switch(month)
            {
                case 9:
                    {
                        if (day <= 22) return ZodiacName.Fecioara;
                        return ZodiacName.Balanta;
                    }
                case 10:
                    {
                        if (day <= 22) return ZodiacName.Balanta;
                        return ZodiacName.Scorpion;
                    }
                case 11:
                    {
                        if (day <= 21) return ZodiacName.Scorpion;
                        return ZodiacName.Sagetator;
                    }
                default: return ZodiacName.Invalid;
            }
        }

        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetAutumnZodiacName(request.Day, request.Month, request.Year)
            });
        }
    }
}
