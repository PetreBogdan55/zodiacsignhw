using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ZodiacMainService
{
    public class MainService : GetZodiacService.GetZodiacServiceBase
    {
        private int day = 0;
        private int month = 0;
        private int year = 0;
        private ServerCallContext context;

        private readonly ILogger<MainService> _logger;
        public MainService(ILogger<MainService> logger)
        {
            _logger = logger;
        }

        private bool CheckForValidDate(string input)
        {
            string[] formats = { "M/d/yyyy", "M/dd/yyyy", "MM/d/yyyy", "MM/dd/yyyy" };
            DateTime dateValue;

            if (DateTime.TryParseExact(input, formats, new CultureInfo("en-US"), DateTimeStyles.None, out dateValue))
                return true;
            else
                return false;
        }
        private void GetDataValues(string input)
        {
            String separator = "/";
            Int32 outputExpectedSubstrings = 3;
            String[] outputStrings = input.Split(separator, outputExpectedSubstrings, StringSplitOptions.RemoveEmptyEntries);
            if (outputStrings.Count() != 3)
                throw new RpcException(new Status(StatusCode.Internal, "Sirul dat nu are un format corespunzator!"));
            int index = 0;
            foreach(string output in outputStrings)
            {
                if(Regex.IsMatch(output, @"^\d+$"))
                {
                    switch(index)
                    {
                        case 0: month = int.Parse(output);break;
                        case 1: day = int.Parse(output);break;
                        case 2: year = int.Parse(output);break;
                        default: break;
                    }
                }
                else
                {
                    throw new RpcException(new Status(StatusCode.Internal, "Sirul dat nu are un format corespunzator!"));
                }
                index++;
            }
        }
        
        private ZodiacName GetZodiacName()
        {
            switch(month)
            {
                case 3:
                case 4:
                case 5:
                    ZodiacSpringService.SpringService springService = new ZodiacSpringService.SpringService(null);
                    var operation1 = springService.GetZodiacDataRequest(new ZodiacSpringService.Date
                    {
                        Day = day,
                        Month = month,
                        Year = year
                    }, context);
                    return (ZodiacName)operation1.Result.ZodiacName;
                case 6:
                case 7:
                case 8:
                    ZodiacSummerService.SummerService summerService = new ZodiacSummerService.SummerService(null);
                    var operation2 = summerService.GetZodiacDataRequest(new ZodiacSummerService.Date
                    {
                        Day = day,
                        Month = month,
                        Year = year
                    }, context);
                    return (ZodiacName)operation2.Result.ZodiacName;
                case 9:
                case 10:
                case 11:
                    ZodiacAutumnService.AutumnService autumnService = new ZodiacAutumnService.AutumnService(null);
                    var operation3 = autumnService.GetZodiacDataRequest(new ZodiacAutumnService.Date
                    {
                        Day = day, Month=month, Year=year
                    }, context);
                    return (ZodiacName)operation3.Result.ZodiacName;
                case 12:
                case 1:
                case 2:
                    ZodiacWinterService.WinterService winterService = new ZodiacWinterService.WinterService(null);
                    var operation4 = winterService.GetZodiacDataRequest(new ZodiacWinterService.Date
                    {
                        Day = day,
                        Month = month,
                        Year = year
                    }, context);
                    return (ZodiacName)operation4.Result.ZodiacName;
                case 0:
                default:
                    // This should not be reached
                    return ZodiacName.Invalid;

            }
        }
        
        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            var replaceDash = new Regex(@"-");
            request.CalendaristicDate = replaceDash.Replace(request.CalendaristicDate, "/");

            this.context = context;

            if(CheckForValidDate(request.CalendaristicDate.ToString()))
                 GetDataValues(request.CalendaristicDate.ToString());
            else throw new RpcException(new Status(StatusCode.Internal, "Sirul dat nu reprezinta o data valida!"));

            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetZodiacName()
            });
        }
    }
}
