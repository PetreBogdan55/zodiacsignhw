using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacSpringService
{
    public class SpringService : GetSpringZodiacService.GetSpringZodiacServiceBase
    {
        private readonly ILogger<SpringService> _logger;
        public SpringService(ILogger<SpringService> logger)
        {
            _logger = logger;
        }

        private ZodiacName GetSpringZodiacName(int day, int month, int year)
        {
            switch (month)
            {
                case 3:
                    {
                        if (day <= 20) return ZodiacName.Pesti;
                        return ZodiacName.Berbec;
                    }
                case 4:
                    {
                        if (day <= 20) return ZodiacName.Berbec;
                        return ZodiacName.Taur;
                    }
                case 5:
                    {
                        if (day <= 20) return ZodiacName.Taur;
                        return ZodiacName.Gemeni;
                    }
                default: return ZodiacName.Invalid;
            }
        }
        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetSpringZodiacName(request.Day, request.Month, request.Year)
            });
        }
    }
}
