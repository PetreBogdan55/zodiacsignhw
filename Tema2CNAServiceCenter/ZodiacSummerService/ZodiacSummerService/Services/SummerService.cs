using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacSummerService
{
    public class SummerService : GetSummerZodiacService.GetSummerZodiacServiceBase
    {
        private readonly ILogger<SummerService> _logger;
        public SummerService(ILogger<SummerService> logger)
        {
            _logger = logger;
        }

        private ZodiacName GetSummerZodiacName(int day, int month, int year)
        {
            switch (month)
            {
                case 6:
                    {
                        if (day <= 21) return ZodiacName.Gemeni;
                        return ZodiacName.Rac;
                    }
                case 7:
                    {
                        if (day <= 22) return ZodiacName.Rac;
                        return ZodiacName.Leu;
                    }
                case 8:
                    {
                        if (day <= 22) return ZodiacName.Leu;
                        return ZodiacName.Fecioara;
                    }
                default: return ZodiacName.Invalid;
            }
        }

        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetSummerZodiacName(request.Day, request.Month, request.Year)
            });
        }
    }
}
