using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacWinterService
{
    public class WinterService : GetWinterZodiacService.GetWinterZodiacServiceBase
    {
        private readonly ILogger<WinterService> _logger;
        public WinterService(ILogger<WinterService> logger)
        {
            _logger = logger;
        }

        private ZodiacName GetWinterZodiacName(int day, int month, int year)
        {
            switch (month)
            {
                case 12:
                    {
                        if (day <= 20) return ZodiacName.Sagetator;
                        return ZodiacName.Capricorn;
                    }
                case 1:
                    {
                        if (day <= 19) return ZodiacName.Capricorn;
                        return ZodiacName.Varsator;
                    }
                case 2:
                    {
                        if (day <= 18) return ZodiacName.Varsator;
                        return ZodiacName.Pesti;
                    }
                default: return ZodiacName.Invalid;
            }
        }
        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetWinterZodiacName(request.Day, request.Month, request.Year)
            });
        }
    }
}
